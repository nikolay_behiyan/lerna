import * as React from 'react';
import {
    View,
    Text
} from 'react-native';

import { MRMRBComponent2 } from 'mrmrb-component2';
import MRMRBUtils from 'mrmrb-utils';

export default class MRMRBComponent extends React.Component<{}, {}> {
    render() {
        return (
            <View>
                <Text>from component 1</Text>
                <MRMRBComponent2 />
            </View>
        );
    }
}